const multiParty = require('multiparty');
const express = require('express');
const fs = require('fs');
const path = require('path');

const PORT = 8080;
var server = express();

const files = {
	sample: {
		path: path.join(__dirname, '/src/sample-knife-party-plur-police.wav'),
		name: 'sample',
		ext: '.wav'
	},
	icone: {
		path: path.join(__dirname, '/src/gitlab-icone.svg'),
		name: 'icone',
		ext: '.svg'
	},
	index: {
		path: path.join(__dirname, '/src/index.html'),
		name: 'index',
		ext: '.html'
	}
}

server.get('/', (req, res) => {
		fs.readFile('./src/index.html', 'utf-8', (err, data) => {
			if (err) {
				res.setHeader({
					'Content-type': 'text/plain'
				});
				res.status(500).send('an error occur');
				return console.error(err);
			}
			res.set({
				'Content-Type': 'text/html'
			});
			res.send(data);
		})
	})
	.get('/:file', (req, res) => {
		let file = {}
		if (req.params.file in files) {
			file = files[req.params.file];
		} else {
			res.setHeader('Content-type', 'text/plain');
			res.status(404).send(req.params.file + ' file not found')
		}
		fs.stat(file.path, (err, stats) => {
			res.header({
				'Content-length': stats.size,
				'Content-Disposition': 'attachement; filename=' + file.name + file.ext
			});
			const stream = fs.createReadStream(file.path)
			stream.pipe(res)
		})
	})
	.post('/upload', (req, res) => {
		var form = new multiParty.Form();
		let count = 0;
		let fileName = '';
		console.log('begin upload')

		form.on('part', (part) => {
			if (!part.filename) {
				console.log('got field named ' + part.name);
				part.on('data', (data) => {
					fileName += data.toString();
				})
				.on('end', () => {
					console.log('fileName : ' + fileName);
				})
				part.resume()
			}

			if (part.filename) {
				count++;
				console.log('got file named ' + part.name);
				console.log('got file named ' + part.filename);
				var fileWrite = fs.createWriteStream('input/' + part.filename)
				console.log('read file ' + part.filename);
				part.on('data', (data) => {
					fileWrite.write(data);
				})
				part.on('end', () => {
					console.log('done')
					fileWrite.end();
				})
				part.resume()
			}

			part.on('error', function (err) {
				console.error(err);
			});
		})
		.on('close', () => {
			console.log('upload done !')
			res.redirect('/');
		})
		.parse(req)
	})
	.use((req, res, next) => {
		res.setHeader('Content-type', 'text/plain');
		res.status(404).send('page not found')
	})

server.listen(PORT, '0.0.0.0', () => console.log('listen on port ' + PORT));
